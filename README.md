- **QuFa.doc** (**Doc**ument of Data **Qu**ality and **Fa**irness)

- 데이터 품질 평가기반 데이터 고도화 및 데이터셋 보정 기술 개발 (Development of data improvement and dataset correction technology based on data quality assessment)

<ul>
    <li>
      <label>QuFa.doc (Repository - structure of directories)</label>
      <ul>
        <li>
          <label>0.monthlyReport - (월간보고서)</label>
        </li>
        <li>
          <label>1.pnu - (부산대학교)</label>
        </li>
        <li>
          <label>2.snu - (서울대학교)</label>
        </li>
        <li>
          <label>3.uos - (서울시립대학교)</label>
        </li>
        <li>
          <label>4.ewu - (이화여자대학교)</label>
        </li>
        <li>
          <label>5.hsu - (한성대학교)</label>
        </li>
        <li>
          <label>6.promptech - ((주)프람트테크놀로지))</label>
        </li>
        <li>
          <label>7.isoft - ((주)아이소프트))</label>
        </li>
      </ul>
    </li>
</ul>
